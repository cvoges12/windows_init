#!/bin/sh

sudo qemu-system-x86_64 \
    -enable-kvm \
    -cpu host \
    -smp 4 \
    -m 4G \
    -boot d \
    -cdrom winsp.iso \
    -vga virtio \
    -monitor stdio
