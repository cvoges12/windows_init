#!/bin/sh

SPICE_PORT=5930
CHARDEV_ID='spicechannel0'
sudo qemu-system-x86_64 \
    -enable-kvm \
    -daemonize \
    -cpu host \
    -smp 4 \
    -net nic \
    -net user,hostname=windowsvm \
    -m 4G \
    -boot d \
    -cdrom Win*.iso \
    -vga qxl \
    -device virtio-serial-pci \
    -spice port=${SPICE_PORT},disable-ticketing=on \
    -device virtserialport,chardev=${CHARDEV_ID},name=com.redhat.spice.0 \
    -chardev spicevmc,id=${CHARDEV_ID},name=vdagent

exec spicy --title Windows 127.0.0.1 -p ${SPICE_PORT}
