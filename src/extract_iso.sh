#!/bin/sh

# mount iso
sudo mkdir -p /mnt/iso
sudo mount -o loop Win10* /mnt/iso

# copy iso files
mkdir -p win
cp -r /mnt/iso/* win/

# clean up mount
sudo umount /mnt/iso
sudo rm -rf /mnt/iso
