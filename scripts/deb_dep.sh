#!/bin/sh

apt update
apt upgrade -y
apt install -y \
    mkisofs \
    qemu-kvm \
    virt-manager \
    virt-viewer \
    libvirt-daemon \
    spice-client-gtk \
    spice-vdagent \
    qemu-utils \
    bridge-utils
